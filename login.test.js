const request = require('supertest');
const { app, server } = require('./app');

describe('POST /login', () => {
    afterAll(() => {
        server.close();
    });

    it('should return 200 OK with a valid token for correct credentials', async () => {
        const response = await request(app)
            .post('/login')
            .send({ username: 'user', password: 'pass' });

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('token', 'xyz');
    });

    it('should return 401 Unauthorized for incorrect credentials', async () => {
        const response = await request(app)
            .post('/login')
            .send({ username: 'user', password: 'wrongpass' });

        expect(response.status).toBe(401);
        expect(response.body).toHaveProperty('error', 'Unauthorized');
    });

    it('should return 400 Bad Request for malformed payloads', async () => {
        // Missing username
        let response = await request(app)
            .post('/login')
            .send({ password: 'pass' });

        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty('error', 'Bad Request');

        // Missing password
        response = await request(app)
            .post('/login')
            .send({ username: 'user' });

        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty('error', 'Bad Request');

        // Missing both username and password
        response = await request(app)
            .post('/login')
            .send({});

        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty('error', 'Bad Request');
    });
});
