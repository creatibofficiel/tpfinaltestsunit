const { Given, When, Then, Before, After } = require('@cucumber/cucumber');
const request = require('supertest');
const { app, server } = require('../../app');
const assert = require('assert');

let response;

Before(() => {
    response = null;
});

After(() => {
    server.close();
});

Given('the user provides valid credentials', function () {
    this.credentials = { username: 'user', password: 'pass' };
});

Given('the user provides invalid credentials', function () {
    this.credentials = { username: 'user', password: 'wrongpass' };
});

When('the user attempts to log in', async function () {
    response = await request(app)
        .post('/login')
        .send(this.credentials);
});

Then('the user should receive a valid token', function () {
    assert.strictEqual(response.status, 200);
    assert(response.body.token);
});

Then('the user should receive an unauthorized error', function () {
    assert.strictEqual(response.status, 401);
    assert(response.body.error);
});
