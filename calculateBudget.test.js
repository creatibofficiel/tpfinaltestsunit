const calculateBudget = require('./calculateBudget');

describe('calculateBudget', () => {
    // Correct calculation of the budget with valid inputs.
    test('calculates budget correctly with valid inputs', () => {
        expect(calculateBudget(1000, [200, 300, 100])).toBe(400);
        expect(calculateBudget(2000, [500, 200, 100])).toBe(1200);
    });

    // Error handling when expenses are not an array.
    test('throws an error if expenses is not an array', () => {
        expect(() => calculateBudget(1000, 'not an array')).toThrow('Expenses must be an array');
        expect(() => calculateBudget(1000, 500)).toThrow('Expenses must be an array');
    });

    // Behavior with empty arrays and negative values.
    test('handles an empty expenses array', () => {
        expect(calculateBudget(1000, [])).toBe(1000);
    });

    test('handles negative values in expenses array', () => {
        expect(calculateBudget(1000, [-100, 200, 300])).toBe(600);
        expect(calculateBudget(1000, [-100, -200, -300])).toBe(1600);
    });
});
